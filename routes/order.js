let express = require("express");
let router = express.Router();

let mongoose = require("mongoose");

let Product = require('../Models/product');
let Order = require('../Models/order');
let User = require('../Models/user');

let ResponseModule = require('../Modules/ResponseModule');
let FilterQuery = require('../Modules/FilterQuery');

/**
 * Get all orders
 */
router.get('/', (req, res, next) => {
    //TODO: make filtering reusable, more efficient
    let filter = {};
    if (req.query) {
        filter = FilterQuery.parse({user: mongoose.Schema.Types.ObjectId, product: mongoose.Schema.Types.ObjectId}, req.query);
    }
    Order.find(filter, (error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        res.send({
            code: 200,
            message: result.length + " order found",
            orders: result
        });
    });
});

/**
 * Get an order
 */
router.get('/:order_id', (req, res, next) => {
    Order.find({_id: req.params.order_id}, (error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        if (result.length === 0)
            return ResponseModule.notFound(res, "User not found");
        res.send({
            code: 200,
            message: "order found",
            order: result[0]
        });
    });
});

/**
 * Make an order
 */
router.post('/', (req, res) => {

    let buyAmount = req.body.quantity ? req.body.quantity : 1;

    User.find({_id: req.body.user_id}, (error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);

        if (result.length === 0)
            return ResponseModule.notFound(res, "User not found");
        
        let user = result[0];

        Product.find({_id: req.body.product_id}, (error, result) => {
            if (error)
                return ResponseModule.internaleServerError(res, "Internal server error", error);
    
            if (result.length === 0)
                return ResponseModule.notFound(res, "Product not found");

            let product = result[0];

            let price = buyAmount * product.price;

            if (product.quantity < buyAmount) {
                //NOTE: not sure what response code to use
                return res.status(200).send({
                    result: "Insufficient product quantity"
                });
            }

            
            if (user.money < price) {
                //NOTE: not sure what response code to use
                return res.status(200).send({
                    result: "Insufficient funds"
                });
            }

            product.quantity -= buyAmount;
            user.money -= price;

            let orderData = {
                _id: new mongoose.Types.ObjectId(),
                user: user,
                product: product,
                quantity: buyAmount,
                createdOn: new Date()
            };

            let order = new Order(orderData);

            order.save((error, result) => {
                if (error)
                    return ResponseModule.internaleServerError(res, "Internal server error", error);
                product.save((error, result) => {
                    if (error)
                        return ResponseModule.internaleServerError(res, "Internal server error", error);
                    user.save((error, result) => {
                        if (error)
                            return ResponseModule.internaleServerError(res, "Internal server error", error);
                        res.status(200).send({
                            result: "Order placed",
                            order: orderData,
                            price: price
                        });
                    });
                });
            });

        });

    });

});

module.exports = router;

/*

There is no native synchronous api for mongodb/mongoose queries (and your wouldn't want one in practicality). 
You should chain the queries, with the second one starting after the first completes and running a callback.
https://stackoverflow.com/a/17181835

 */