let express = require("express");
let router = express.Router();

let mongoose = require("mongoose");

let User = require('../Models/user');

let ResponseModule = require('../Modules/ResponseModule');

let FilterQuery = require('../Modules/FilterQuery');

/**
 * Get all users
 */
router.get('/', (req, res, next) => {
    //TODO: make filtering reusable, more efficient
    let filter = {};
    if (req.query) {
        filter = FilterQuery.parse({firstName: String, lastName: String, money: Number}, req.query);
    }
    User.find(filter, (error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        res.send({
            code: 200,
            message: result.length + " users found",
            users: result
        });
    });
});

/**
 * Get a user
 */
router.get('/:user_id', (req, res, next) => {
    User.find({_id: req.params.user_id}, (error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        if (result.length === 0)
            return ResponseModule.notFound(res, "User not found");
        res.send({
            code: 200,
            message: "user found",
            user: result[0]
        });
    });
});

/**
 * Create a user
 */
router.post('/', (req, res, next) => {
    let missing_parameters = [];
    if (req.body.firstName === undefined) {
        missing_parameters.push('firstName');
    }

    if (req.body.lastName === undefined) {
        missing_parameters.push('lastName');
    }

    if (req.body.money === undefined) {
        missing_parameters.push('money');
    }
    if (missing_parameters.length > 0) {
        return res.status(400).send({
            code: 400,
            message: "Missing required parameters",
            missing_parameters: missing_parameters
        });
    }
    let userData = {
        _id: new mongoose.Types.ObjectId(),
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        money: req.body.money
    }
    let user = new User(userData);
    user.save((error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        res.send({
            code: 200,
            message: "User created",
            user: result
        });
    });
});

/**
 * Update a user
 */
router.put('/:user_id', (req, res, next) => {
    
    User.find({ _id: req.params.user_id}, (error, users) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);

        if (users.length === 0)
            return ResponseModule.notFound(res, "User not found");

        let user = users[0];

        if (req.body.firstName) {
            user.firstName = req.body.firstName;
        }
        if (req.body.lastName) {
            user.lastName = req.body.lastName;
        }
        if (req.body.money) {
            user.money = req.body.money;
        }

        user.save((error, result) => {
            if(error)
                return ResponseModule.internaleServerError(res, "Internal server error", error);
            res.send({
                code: 200,
                message: "User updated",
                user: result
            });
        });
    });
});

/**
 * Delete a user
 */
router.delete('/:user_id', (req, res, next) => {
    
    User.find({ _id: req.params.user_id}, (error, users) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);

        if (users.length === 0)
            return ResponseModule.notFound(res, "User not found");

        let user = users[0];

        user.delete((error, result) => {
            if (error)
                return ResponseModule.internaleServerError(res, "Internal server error", error);
            res.send({
                code: 200,
                message: "User deleted",
                user: result
            });
        });
    });
});







module.exports = router;