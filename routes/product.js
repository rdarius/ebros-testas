let express = require("express");
let router = express.Router();

let mongoose = require("mongoose");

let Product = require('../Models/product');

let ResponseModule = require('../Modules/ResponseModule');

let productTypes = ["furniture", "food", "clothes"];

/**
 * Get all products
 */
router.get('/', (req, res, next) => {
    //TODO: add filters
    Product.find({}, (error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        res.send({
            code: 200,
            message: result.length + " products found",
            products: result
        });
    });
});

/**
 * Get a product
 */
router.get('/:product_id', (req, res, next) => {
    Product.find({_id: req.params.product_id}, (error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        if (result.length === 0)
            return ResponseModule.notFound(res, "Product not found");
        res.send({
            code: 200,
            message: "product found",
            product: result[0]
        });
    });
});

/**
 * Create a product
 */
router.post('/', (req, res, next) => {
    let missing_parameters = [];
    if (req.body.title === undefined) {
        missing_parameters.push('title');
    }

    if (req.body.type === undefined) {
        missing_parameters.push('type');
    }

    if (req.body.url === undefined) {
        missing_parameters.push('url');
    }

    if (req.body.quantity === undefined) {
        missing_parameters.push('quantity');
    }

    if (req.body.price === undefined) {
        missing_parameters.push('price');
    }

    if (missing_parameters.length > 0) {
        return res.status(400).send({
            code: 400,
            message: "Missing required parameters",
            missing_parameters: missing_parameters
        });
    }

    if (productTypes.indexOf(req.body.type) === -1)
        return ResponseModule.badRequest(res, "Invalid value of parameter `type`");

    let productData = {
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        type: req.body.type,
        url: req.body.url,
        quantity: req.body.quantity,
        price: req.body.price
    };
    let product = new Product(productData);
    product.save((error, result) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);
        res.send({
            code: 200,
            message: "Product created",
            product: result
        });
    });
});

/**
 * Update a user
 */
router.put('/:product_id', (req, res, next) => {
    
    Product.find({ _id: req.params.product_id}, (error, products) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);

        if (products.length === 0)
            return ResponseModule.notFound(res, "Product not found");

        let product = products[0];

        if (req.body.title) {
            product.title = req.body.title;
        }
        if (req.body.type) {
            if (productTypes.indexOf(req.body.type) === -1)
                return ResponseModule.badRequest(res, "Invalid value of parameter `type`");
            product.type = req.body.type;
        }
        if (req.body.url) {
            product.url = req.body.url;
        }
        if (req.body.quantity) {
            product.quantity = req.body.quantity;
        }
        if (req.body.price) {
            product.price = req.body.price;
        }

        product.save((error, result) => {
            if (error)
                return ResponseModule.internaleServerError(res, "Internal server error", error);
            res.send({
                code: 200,
                message: "Product updated",
                product: result
            });
        });
    });
});

/**
 * Delete a user
 */
router.delete('/:product_id', (req, res, next) => {
    
    Product.find({ _id: req.params.product_id}, (error, products) => {
        if (error)
            return ResponseModule.internaleServerError(res, "Internal server error", error);

        if (products.length === 0)
            return ResponseModule.notFound(res, "Product not found");

        let product = products[0];

        product.delete((error, result) => {
            if (error)
                return ResponseModule.internaleServerError(res, "Internal server error", error);
            res.send({
                code: 200,
                message: "Product deleted",
                productg: result
            });
        });
    });
});







module.exports = router;