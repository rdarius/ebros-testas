
let Product = require('../Models/product');
let Order = require('../Models/order');
let User = require('../Models/user');

let mongoose = require('mongoose');

module.exports = class FilterQuery {

    //TODO: Better support for various data types

    static parse (map, query) {

        let filter = {};

        for (let key in map) {
            if (!map.hasOwnProperty(key)) continue;

            if (!query[key]) continue;

            if (map[key] === Number) {
                //TODO: implement $gt: >, $lt: < $gte: >=, $lte <=
                filter[key] = query[key];
            }

            if (map[key] === mongoose.Schema.Types.ObjectId) {
                filter[key] = mongoose.Types.ObjectId(query[key]);
            }
            
            if (map[key] === String) {
                // TODO: implement more detailed search
                filter[key] = {
                    $regex: new RegExp("^" + query[key], "i") // case insensitive, has to start with string given
                };
            }
        }

        return filter;

    }

}