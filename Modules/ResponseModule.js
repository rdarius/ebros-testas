module.exports = class ResponseModule {

    static badRequest (response, message, error) {
        // TODO: Log error if there is one
        response.status(400).send({
            code: 400,
            message: message
        });
    }

    static forbidden (response, message, error) {
        // TODO: Log error if there is one
        response.status(403).send({
            code: 403,
            message: message
        });
    }

    static notFound (response, message, error) {
        // TODO: Log error if there is one
        response.status(404).send({
            code: 404,
            message: message
        });
    }

    static internaleServerError (response, message, error) {
        // TODO: Log error if there is one
        response.status(500).send({
            code: 500,
            message: message
        });
    }

};