// Loading environment variables
require('dotenv').config();

let express = require('express');
let app = express();

// Cross-origin resource sharing
let cors = require('cors');

let mongoose = require('mongoose');

let ResponseModule = require('./Modules/ResponseModule');

let Product = require('./Models/product');
let Order = require('./Models/order');


let port = 8888;

let useAuthorization = false;

mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true});

app.use(express.json());
app.use(cors());

app.use((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');

    if(useAuthorization && process.env.AUTHORIZATION && req.headers.authorization !== process.env.AUTHORIZATION)
        return ResponseModule.forbidden(res, "Unauthorized");

    next();
});

app.use('/user', require('./routes/user'));
app.use('/product', require('./routes/product'));
app.use('/order', require('./routes/order'));

app.use((req, res) => {
    ResponseModule.badRequest(res, "Ivalid route");
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});